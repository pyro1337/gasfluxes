This is a package for calculation of greenhouse gas fluxes from soils, which were measured using chambers.

The package is [available from CRAN](https://cran.r-project.org/package=gasfluxes) and you can install as usual:

```
#!r

install.packages("gasfluxes")
```


If you'd like to install the development version you can use:


```
#!r

install.packages(c("sfsmisc",
                 "data.table",
                 "MASS",
                 "AICcmodavg"))
library(remotes)
install_bitbucket("ecoRoland/gasfluxes")
```


If you use MS Windows, you must follow the instructions in the ["R Administration and Installation" manual](http://cran.r-project.org/doc/manuals/R-admin.html#The-Windows-toolset) to be able to build packages.

Contributions, bug reports and feature requests are always welcome.